# Cohesive Zone Data

## Figure files and data associated with the manuscript, `Measurement of crack tip kinematics in a brittle hydrogel reveals the cohesive zone structure.'

In this repository, you'll find the figures and some data that appear in the manuscript.

## Authors 

Chenzhuo Li (EPFL), Xinyue Wei (EPFL), Meng Wang (HUJI), Mokhtar Adda-Bedia (ENS-Lyon), John Martin Kolinski (EPFL)

## Acknowledgements

JMK gratefully acknowledges SNSF grant no. 200021_197162 for supporting this work.

## License
These data are intended to be shared according to the PNAS Materials and Data Availability Policy. Please handle these data according to the open data commons attribution (ODC-By) license, http://opendatacommons.org/licenses/by/1.0/


